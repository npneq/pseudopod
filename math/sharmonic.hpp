/* -*- indent-tabs-mode: t -*- */

#ifndef MATH_SHARMONIC
#define MATH_SHARMONIC

// Copyright (C) 2019-2023 Lawrence Livermore National Security, LLC., Xavier Andrade
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <complex>

//#define GENERATE_REFERENCE

#ifdef GENERATE_REFERENCE
#include <boost/math/special_functions/spherical_harmonic.hpp>
#endif

#define SHARMONIC_MAX_L 10

namespace pseudo {
namespace math {

#include <gpu/run.hpp>

GPU_FUNCTION double norm(double xx, double yy, double zz) {
	xx = fabs(xx);
	yy = fabs(yy);
	zz = fabs(zz);

	auto swap = [] GPU_FUNCTION (double & aa, double & bb) {
		auto cc = aa;
		aa = bb;
		bb = cc;
	};
	
	if (xx < yy) swap(xx, yy);
	if (yy < zz) swap(yy, zz);
	if (xx < yy) swap(xx, yy);

	if(xx == 0.0) return 0.0;
	
	yy /= xx;
	zz /= xx;

	return xx*sqrt(1.0 + yy*yy + zz*zz);
}

GPU_FUNCTION auto normalize(double & xx, double & yy, double & zz) {
	auto nn = norm(xx, yy, zz);
	if(nn == 0.0) return 0.0;
	
	xx /= nn;
	yy /= nn;
	zz /= nn;
	return nn;
}

GPU_FUNCTION auto sharmonic_real_normalized(int const ll, int const mm, double xx, double yy, double zz) {
	assert(ll >= 0);
	assert(ll <= SHARMONIC_MAX_L);
	assert(abs(mm) <= ll);

	// Functions taken from:
	//   [1] https://en.wikipedia.org/wiki/Table_of_spherical_harmonics
	//   [2] https://www.doiserbia.nb.rs/img/doi/1450-698X/2009/1450-698X0979107M.pdf
	//   [3] https://github.com/elerac/sh_table/blob/main/docs/table.txt
	
	auto xx2 = xx*xx;
	auto yy2 = yy*yy;
	auto xx4 = xx2*xx2;
	auto yy4 = yy2*yy2;
	auto xx6 = xx4*xx2;
	auto yy6 = yy4*yy2;
	auto xx8 = xx4*xx4;
	auto yy8 = yy4*yy4;
	auto zz2 = zz*zz;
	auto zz3 = zz2*zz;
	auto zz4 = zz2*zz2;
	
	switch(ll){
	case 0:
		return 1.0/sqrt(4.0*M_PI);
	case 1:
		if(mm == -1) return sqrt(3.0/(4.0*M_PI))*yy;
		if(mm ==	0) return sqrt(3.0/(4.0*M_PI))*zz;
		if(mm ==	1) return sqrt(3.0/(4.0*M_PI))*xx;
		return 0.0;
	case 2:
		if(mm == -2) return 1.0/2.0*sqrt(15.0/M_PI)*xx*yy;
		if(mm == -1) return 1.0/2.0*sqrt(15.0/M_PI)*yy*zz;
		if(mm ==	0) return 1.0/4.0*sqrt(5.0/M_PI)*(3.0*zz2 - 1.0);
		if(mm ==	1) return 1.0/2.0*sqrt(15.0/M_PI)*xx*zz;
		if(mm ==	2) return 1.0/4.0*sqrt(15.0/M_PI)*(xx2 - yy2);
		return 0.0;
	case 3:
		if(mm == -3) return 1.0/4.0*sqrt(35.0/(2.0*M_PI))*yy*(3.0*xx2 - yy2);
		if(mm == -2) return 1.0/2.0*sqrt(105.0/M_PI)*xx*yy*zz;
		if(mm == -1) return 1.0/4.0*sqrt(21.0/(2.0*M_PI))*yy*(5.0*zz2 - 1.0);
		if(mm ==	0) return 1.0/4.0*sqrt(7.0/M_PI)*(5.0*zz2 - 3.0)*zz;
		if(mm ==	1) return 1.0/4.0*sqrt(21.0/(2.0*M_PI))*xx*(5.0*zz2 - 1.0);
		if(mm ==	2) return 1.0/4.0*sqrt(105.0/M_PI)*(xx2 - yy2)*zz;
		if(mm ==	3) return 1.0/4.0*sqrt(35.0/(2.0*M_PI))*xx*(xx2 - 3.0*yy2);
		return 0.0;
	case 4:
		if(mm == -4) return 3.0/4.0*sqrt(35.0/M_PI)*xx*yy*(xx2 - yy2);
		if(mm == -3) return 3.0/4.0*sqrt(35.0/(2.0*M_PI))*yy*(3.0*xx2 - yy2)*zz;
		if(mm == -2) return 3.0/4.0*sqrt(5.0/M_PI)*xx*yy*(7.0*zz2 - 1.0);
		if(mm == -1) return 3.0/4.0*sqrt(5.0/(2.0*M_PI))*yy*(7.0*zz3 - 3.0*zz);
		if(mm ==	0) return 3.0/16.0*sqrt(1.0/M_PI)*(35.0*zz4 - 30.0*zz2 + 3.0);
		if(mm ==	1) return 3.0/4.0*sqrt(5.0/(2.0*M_PI))*xx*(7.0*zz3 - 3.0*zz);		
		if(mm ==	2) return 3.0/8.0*sqrt(5.0/M_PI)*(xx2 - yy2)*(7.0*zz2 - 1.0);		
		if(mm ==	3) return 3.0/4.0*sqrt(35.0/(2.0*M_PI))*xx*(xx2 - 3.0*yy2)*zz;		
		if(mm ==	4) return 3.0/16.0*sqrt(35.0/M_PI)*(xx2*(xx2 - 3.0*yy2) - yy2*(3.0*xx2-yy2));
		return 0.0;
	case 5:
		if(mm == -5) return sqrt(2.0)*3.0/32.0*sqrt(77.0/M_PI)*yy*(5.0*xx4 - 10.0*yy2*xx2 + yy4);
		if(mm == -4) return -sqrt(2.0)*3.0/8.0*sqrt(770.0/M_PI)*(yy2 - xx2)*yy*zz*xx;
		if(mm == -3) return sqrt(2.0)*1.0/32.0*sqrt(385.0/M_PI)*(yy2 + xx2 - 8.0*zz2)*(-3.0*xx2 + yy2)*yy;
		if(mm == -2) return -sqrt(2.0)*1.0/8.0*sqrt(2310.0/M_PI)*yy*(-2.0*zz2 + yy2 + xx2)*zz*xx;
		if(mm == -1) return sqrt(2.0)*1.0/32.0*sqrt(330.0/M_PI)*yy*(xx4 - 12.0*zz2*xx2 + 2.0*yy2*xx2 + 8.0*zz4 - 12.0*yy2*zz2 + yy4);
		if(mm ==	0) return 1.0/16.0*sqrt(11.0/M_PI)*(8.0*zz4 - 40.0*zz2*xx2 - 40.0*yy2*zz2 + 30.0*yy2*xx2 + 15.0*xx4 + 15.0*yy4)*zz;
		if(mm ==	1) return sqrt(2.0)*1.0/32.0*sqrt(330.0/M_PI)*(xx4 - 12.0*zz2*xx2 + 2.0*yy2*xx2 + 8.0*zz4 - 12.0*yy2*zz2 + yy4)*xx;
		if(mm ==	2) return sqrt(2.0)*1.0/16.0*sqrt(2310.0/M_PI)*(-2.0*zz2 + yy2 + xx2)*(yy2 - xx2)*zz;		
		if(mm ==	3) return sqrt(2.0)*1.0/32.0*sqrt(385.0/M_PI)*(yy2 + xx2 - 8.0*zz2)*(-xx2 + 3.0*yy2)*xx;		
		if(mm ==	4) return sqrt(2.0)*3.0/32.0*sqrt(770.0/M_PI)*(-xx2 - 2*yy*xx + yy2)*(-xx2 + 2.0*xx*yy + yy2)*zz;
		if(mm ==	5) return sqrt(2.0)*3.0/32.0*sqrt(77.0/M_PI)*(xx4 - 10.0*yy2*xx2 + 5.0*yy4)*xx;
		return 0.0;
	case 6:
		if(mm == -6) return 0.0078125*sqrt(6006/M_PI)*xx*yy*(12.0*xx4 - 40.0*xx2*yy2 + 12.0*yy4);
		if(mm == -5) return 0.015625*sqrt(2002/M_PI)*yy*zz*(30.0*xx4 - 60.0*xx2*yy2 + 6.0*yy4);
		if(mm == -4) return 0.015625*sqrt(91/M_PI)*xx*yy*(240.0*xx2 - 240.0*yy2 - 264.0*xx4 + 264.0*yy4);
		if(mm == -3) return 0.015625*sqrt(2730/M_PI)*yy*zz*(48.0*xx2 - 16.0*yy2 - 66.0*xx4 - 44.0*xx2*yy2 + 22.0*yy4);
		if(mm == -2) return 0.0078125*sqrt(2730/M_PI)*xx*yy*(64.0 - 192.0*xx2 - 192.0*yy2 + 132.0*xx4 + 264.0*xx2*yy2 + 132.0*yy4);
		if(mm == -1) return 0.0625*sqrt(273/M_PI)*yy*zz*(8 - 36*xx2 - 36*yy2 + 33*xx4 + 66*xx2*yy2 + 33*yy4);
		if(mm ==  0) return 1.0/32.0*sqrt(13/M_PI)*(16 - 168*xx2 - 168*yy2 + 378*xx4 + 756*xx2*yy2 + 378*yy4 - 231*xx6 - 693*xx4*yy2 - 693*xx2*yy4 - 231*yy6);
		if(mm ==  1) return 1.0/16.0*sqrt(273/M_PI)*xx*zz*(8 - 36*xx2 - 36*yy2 + 33*xx4 + 66*xx2*yy2 + 33*yy4);
		if(mm ==  2) return 1.0/64.0*sqrt(2730/M_PI)*(xx - yy)*(xx + yy)*(16 - 48*xx2 - 48*yy2 + 33*xx4 + 66*xx2*yy2 + 33*yy4);
		if(mm ==  3) return -1.0/32.0*sqrt(2730/M_PI)*xx*zz*(xx2 - 3*yy2)*(-8 + 11*xx2 + 11*yy2);
		if(mm ==  4) return 3.0/32.0*sqrt(91/M_PI)*(-10 + 11*xx2 + 11*yy2)*(-xx2 + 2*xx*yy + yy2)*(xx2 + 2*xx*yy - yy2);
		if(mm ==  5) return 3.0/32.0*sqrt(2002/M_PI)*xx*zz*(xx4 - 10*xx2*yy2 + 5*yy4);
		if(mm ==  6) return 1.0/64.0*sqrt(6006/M_PI)*(xx6 - 15*xx4*yy2 + 15*xx2*yy4 - yy6);
		return 0.0;
	case 7:
		if(mm == -7) return 0.0078125*sqrt(715/M_PI)*yy*(42.0*xx6 - 210.0*xx4*yy2 + 126.0*xx2*yy4 - 6.0*yy6);
		if(mm == -6) return 0.0078125*sqrt(10010/M_PI)*xx*yy*zz*(36.0*xx4 - 120.0*xx2*yy2 + 36.0*yy4);
		if(mm == -5) return 0.0078125*sqrt(385/M_PI)*yy*(360.0*xx4 - 720.0*xx2*yy2 + 72.0*yy4 - 390.0*xx6 + 390.0*xx4*yy2 + 702.0*xx2*yy4 - 78.0*yy6);
		if(mm == -4) return 0.015625*sqrt(385/M_PI)*xx*yy*zz*(240.0*xx2 - 240.0*yy2 - 312.0*xx4 + 312.0*yy4);
		if(mm == -3) return 0.0078125*sqrt(35/M_PI)*yy*(1440.0*xx2 - 480.0*yy2 - 3960.0*xx4 - 2640.0*xx2*yy2 + 1320.0*yy4 + 2574.0*xx6 + 4290.0*xx4*yy2 + 858.0*xx2*yy4 - 858.0*yy6);
		if(mm == -2) return 0.0078125*sqrt(70/M_PI)*xx*yy*zz*(576.0 - 2112.0*xx2 - 2112.0*yy2 + 1716.0*xx4 + 3432.0*xx2*yy2 + 1716.0*yy4);
		if(mm == -1) return 1.0*sqrt(105/M_PI)*yy*(1.0 - 6.75*xx2 - 6.75*yy2 + 12.375*xx4 + 24.75*xx2*yy2 + 12.375*yy4 - 6.703125*xx6 - 20.109375*xx4*yy2 - 20.109375*xx2*yy4 - 6.703125*yy6);
		if(mm ==  0) return 1.0/32.0*sqrt(15/M_PI)*zz*(16 - 216*xx2 - 216*yy2 + 594*xx4 + 1188*xx2*yy2 + 594*yy4 - 429*xx6 - 1287*xx4*yy2 - 1287*xx2*yy4 - 429*yy6);
		if(mm ==  1) return 1.0/64.0*sqrt(105/M_PI)*xx*(64 - 432*xx2 - 432*yy2 + 792*xx4 + 1584*xx2*yy2 + 792*yy4 - 429*xx6 - 1287*xx4*yy2 - 1287*xx2*yy4 - 429*yy6);
		if(mm ==  2) return 3.0/64.0*sqrt(70/M_PI)*zz*(xx - yy)*(xx + yy)*(48 - 176*xx2 - 176*yy2 + 143*xx4 + 286*xx2*yy2 + 143*yy4);
		if(mm ==  3) return 3.0/64.0*sqrt(35/M_PI)*xx*(xx2 - 3*yy2)*(80 - 220*xx2 - 220*yy2 + 143*xx4 + 286*xx2*yy2 + 143*yy4);
		if(mm ==  4) return 3.0/32.0*sqrt(385/M_PI)*zz*(-10 + 13*xx2 + 13*yy2)*(-xx2 + 2*xx*yy + yy2)*(xx2 + 2*xx*yy - yy2);
		if(mm ==  5) return -3.0/64.0*sqrt(385/M_PI)*xx*(-12 + 13*xx2 + 13*yy2)*(xx4 - 10*xx2*yy2 + 5*yy4);
		if(mm ==  6) return 3.0/64.0*sqrt(10010/M_PI)*zz*(xx6 - 15*xx4*yy2 + 15*xx2*yy4 - yy6);
		if(mm ==  7) return 3.0/64.0*sqrt(715/M_PI)*xx*(xx6 - 21*xx4*yy2 + 35*xx2*yy4 - 7*yy6);
		return 0.0;
	case 8:
		if(mm == -8) return 0.001953125*sqrt(12155/M_PI)*xx*yy*(48.0*xx6 - 336.0*xx4*yy2 + 336.0*xx2*yy4 - 48.0*yy6);
		if(mm == -7) return 0.0078125*sqrt(12155/M_PI)*yy*zz*(42.0*xx6 - 210.0*xx4*yy2 + 126.0*xx2*yy4 - 6.0*yy6);
		if(mm == -6) return 0.00390625*sqrt(14586/M_PI)*xx*yy*(168.0*xx4 - 560.0*xx2*yy2 + 168.0*yy4 - 180.0*xx6 + 420.0*xx4*yy2 + 420.0*xx2*yy4 - 180.0*yy6);
		if(mm == -5) return 0.0078125*sqrt(17017/M_PI)*yy*zz*(120.0*xx4 - 240.0*xx2*yy2 + 24.0*yy4 - 150.0*xx6 + 150.0*xx4*yy2 + 270.0*xx2*yy4 - 30.0*yy6);
		if(mm == -4) return 0.00390625*sqrt(1309/M_PI)*xx*yy*(960.0*xx2 - 960.0*yy2 - 2496.0*xx4 + 2496.0*yy4 + 1560.0*xx6 + 1560.0*xx4*yy2 - 1560.0*xx2*yy4 - 1560.0*yy6);
		if(mm == -3) return 0.0078125*sqrt(19635/M_PI)*yy*zz*(96.0*xx2 - 32.0*yy2 - 312.0*xx4 - 208.0*xx2*yy2 + 104.0*yy4 + 234.0*xx6 + 390.0*xx4*yy2 + 78.0*xx2*yy4 - 78.0*yy6);
		if(mm == -2) return 0.00390625*sqrt(1190/M_PI)*xx*yy*(384.0 - 2112.0*xx2 - 2112.0*yy2 + 3432.0*xx4 + 6864.0*xx2*yy2 + 3432.0*yy4 - 1716.0*xx6 - 5148.0*xx4*yy2 - 5148.0*xx2*yy4 - 1716.0*yy6);
		if(mm == -1) return sqrt(17/M_PI)*yy*zz*(3.0 - 24.75*xx2 - 24.75*yy2 + 53.625*xx4 + 107.25*xx2*yy2 + 53.625*yy4 - 33.515625*xx6 - 100.546875*xx4*yy2 - 100.546875*xx2*yy4 - 33.515625*yy6);
		if(mm ==  0) return 1.0/256.0*sqrt(17/M_PI)*(128 - 2304*xx2 - 2304*yy2 + 9504*xx4 + 19008*xx2*yy2 + 9504*yy4 - 13728*xx6 - 41184*xx4*yy2
																								 - 41184*xx2*yy4 - 13728*yy6 + 6435*xx8 + 25740*xx6*yy2 + 38610*xx4*yy4 + 25740*xx2*yy6 + 6435*yy8);
		if(mm ==  1) return 3.0/64.0*sqrt(17/M_PI)*xx*zz*(64 - 528*xx2 - 528*yy2 + 1144*xx4 + 2288*xx2*yy2 + 1144*yy4 - 715*xx6 - 2145*xx4*yy2 - 2145*xx2*yy4 - 715*yy6);
		if(mm ==  2) return -3.0/128.0*sqrt(1190/M_PI)*(xx - yy)*(xx + yy)*(-32 + 176*xx2 + 176*yy2 - 286*xx4 - 572*xx2*yy2 - 286*yy4 + 143*xx6 + 429*xx4*yy2 + 429*xx2*yy4 + 143*yy6);
		if(mm ==  3) return 1.0/64.0*sqrt(19635/M_PI)*xx*zz*(xx2 - 3*yy2)*(16 - 52*xx2 - 52*yy2 + 39*xx4 + 78*xx2*yy2 + 39*yy4);
		if(mm ==  4) return -3.0/128.0*sqrt(1309/M_PI)*(-xx2 + 2*xx*yy + yy2)*(xx2 + 2*xx*yy - yy2)*(40 - 104*xx2 - 104*yy2 + 65*xx4 + 130*xx2*yy2 + 65*yy4);
		if(mm ==  5) return -3.0/64.0*sqrt(17017/M_PI)*xx*zz*(-4 + 5*xx2 + 5*yy2)*(xx4 - 10*xx2*yy2 + 5*yy4);
		if(mm ==  6) return -1.0/128.0*sqrt(14586/M_PI)*(xx - yy)*(xx + yy)*(-14 + 15*xx2 + 15*yy2)*(xx2 - 4*xx*yy + yy2)*(xx2 + 4*xx*yy + yy2);
		if(mm ==  7) return 3.0/64.0*sqrt(12155/M_PI)*xx*zz*(xx6 - 21*xx4*yy2 + 35*xx2*yy4 - 7*yy6);
		if(mm ==  8) return 3.0/256.0*sqrt(12155/M_PI)*(xx8 - 28*xx6*yy2 + 70*xx4*yy4 - 28*xx2*yy6 + yy8);
		return 0.0;
	case 9:
		if(mm == -9) return 0.0009765625*sqrt(461890/M_PI)*yy*(18.0*xx8 - 168.0*xx6*yy2 + 252.0*xx4*yy4 - 72.0*xx2*yy6 + 2.0*yy8);
		if(mm == -8) return 0.001953125*sqrt(230945/M_PI)*xx*yy*zz*(48.0*xx6 - 336.0*xx4*yy2 + 336.0*xx2*yy4 - 48.0*yy6);
		if(mm == -7) return 0.0009765625*sqrt(27170/M_PI)*yy*(672.0*xx6 - 3360.0*xx4*yy2 + 2016.0*xx2*yy4 - 96.0*yy6 - 714.0*xx8
																													+ 2856.0*xx6*yy2 + 1428.0*xx4*yy4 - 2040.0*xx2*yy6 + 102.0*yy8);
		if(mm == -6) return 0.00390625*sqrt(81510/M_PI)*xx*yy*zz*(168.0*xx4 - 560.0*xx2*yy2 + 168.0*yy4 - 204.0*xx6 + 476.0*xx4*yy2 + 476.0*xx2*yy4 - 204.0*yy6);
		if(mm == -5) return 0.001953125*sqrt(5434/M_PI)*yy*(1680.0*xx4 - 3360.0*xx2*yy2 + 336.0*yy4 - 4200.0*xx6 + 4200.0*xx4*yy2 + 7560.0*xx2*yy4
																												- 840.0*yy6 + 2550.0*xx8 - 7140.0*xx4*yy4 - 4080.0*xx2*yy6 + 510.0*yy8);
		if(mm == -4) return 0.00390625*sqrt(95095/M_PI)*xx*yy*zz*(192.0*xx2 - 192.0*yy2 - 576.0*xx4 + 576.0*yy4 + 408.0*xx6 + 408.0*xx4*yy2 - 408.0*xx2*yy4 - 408.0*yy6);
		if(mm == -3) return 0.001953125*sqrt(43890/M_PI)*yy*(384.0*xx2 - 128.0*yy2 - 1872.0*xx4 - 1248.0*xx2*yy2 + 624.0*yy4 + 2808.0*xx6 + 4680.0*xx4*yy2
																												 + 936.0*xx2*yy4 - 936.0*yy6 - 1326.0*xx8 - 3536.0*xx6*yy2 - 2652.0*xx4*yy4 + 442.0*yy8);
		if(mm == -2) return 0.00390625*sqrt(2090/M_PI)*xx*yy*zz*(384.0 - 2496.0*xx2 - 2496.0*yy2 + 4680.0*xx4 + 9360.0*xx2*yy2 + 4680.0*yy4
																														 - 2652.0*xx6 - 7956.0*xx4*yy2 - 7956.0*xx2*yy4 - 2652.0*yy6);
		if(mm == -1) return 0.01171875*sqrt(95/M_PI)*yy*(128 - 1408*xx2 - 1408*yy2 + 4576*xx4 + 9152*xx2*yy2 + 4576*yy4 - 5720*xx6 - 17160*xx4*yy2
																										 - 17160*xx2*yy4 - 5720*yy6 + 2431*xx8 + 9724*xx6*yy2 + 14586*xx4*yy4 + 9724*xx2*yy6 + 2431*yy8);
		if(mm ==  0) return 1.0/256.0*sqrt(19/M_PI)*zz*(128 - 2816*xx2 - 2816*yy2 + 13728*xx4 + 27456*xx2*yy2 + 13728*yy4 - 22880*xx6 - 68640*xx4*yy2
																										 - 68640*xx2*yy4 - 22880*yy6 + 12155*xx8 + 48620*xx6*yy2 + 72930*xx4*yy4 + 48620*xx2*yy6 + 12155*yy8);
		if(mm ==  1) return 3.0/256.0*sqrt(95/M_PI)*xx*(128 - 1408*xx2 - 1408*yy2 + 4576*xx4 + 9152*xx2*yy2 + 4576*yy4 - 5720*xx6 - 17160*xx4*yy2
																										- 17160*xx2*yy4 - 5720*yy6 + 2431*xx8 + 9724*xx6*yy2 + 14586*xx4*yy4 + 9724*xx2*yy6 + 2431*yy8);
		if(mm ==  2) return -3.0/128*sqrt(2090/M_PI)*zz*(xx - yy)*(xx + yy)*(-32 + 208*xx2 + 208*yy2 - 390*xx4 - 780*xx2*yy2 - 390*yy4 + 221*xx6 + 663*xx4*yy2 + 663*xx2*yy4 + 221*yy6);
		if(mm ==  3) return -1.0/256.0*sqrt(43890/M_PI)*xx*(xx2 - 3*yy2)*(-64 + 312*xx2 + 312*yy2 - 468*xx4 - 936*xx2*yy2 - 468*yy4 + 221*xx6 + 663*xx4*yy2 + 663*xx2*yy4 + 221*yy6);
		if(mm ==  4) return -3.0/128.0*sqrt(95095/M_PI)*zz*(-xx2 + 2*xx*yy + yy2)*(xx2 + 2*xx*yy - yy2)*(8 - 24*xx2 - 24*yy2 + 17*xx4 + 34*xx2*yy2 + 17*yy4);
		if(mm ==  5) return 3.0/256.0*sqrt(5434/M_PI)*xx*(xx4 - 10*xx2*yy2 + 5*yy4)*(56 - 140*xx2 - 140*yy2 + 85*xx4 + 170*xx2*yy2 + 85*yy4);
		if(mm ==  6) return -1.0/128.0*sqrt(81510/M_PI)*zz*(xx - yy)*(xx + yy)*(-14 + 17*xx2 + 17*yy2)*(xx2 - 4*xx*yy + yy2)*(xx2 + 4*xx*yy + yy2);
		if(mm ==  7) return -3.0/512.0*sqrt(27170/M_PI)*xx*(-16 + 17*xx2 + 17*yy2)*(xx6 - 21*xx4*yy2 + 35*xx2*yy4 - 7*yy6);
		if(mm ==  8) return 3.0/256.0*sqrt(230945/M_PI)*zz*(xx8 - 28*xx6*yy2 + 70*xx4*yy4 - 28*xx2*yy6 + yy8);
		if(mm ==  9) return 1.0/512.0*sqrt(461890/M_PI)*xx*(xx2 - 3*yy2)*(xx6 - 33*xx4*yy2 + 27*xx2*yy4 - 3*yy6);
		return 0.0;
	case 10:
		if(mm == -10) return 0.00048828125*sqrt(1939938/M_PI)*xx*yy*(20.0*xx8 - 240.0*xx6*yy2 + 504.0*xx4*yy4 - 240.0*xx2*yy6 + 20.0*yy8);
		if(mm == -9) return 0.0009765625*sqrt(9699690/M_PI)*yy*zz*(18.0*xx8 - 168.0*xx6*yy2 + 252.0*xx4*yy4 - 72.0*xx2*yy6 + 2.0*yy8);
		if(mm == -8) return 0.0009765625*sqrt(255255/M_PI)*xx*yy*(288.0*xx6 - 2016.0*xx4*yy2 + 2016.0*xx2*yy4 - 288.0*yy6 - 304.0*xx8 + 1824.0*xx6*yy2 - 1824.0*xx2*yy6 + 304.0*yy8);
		if(mm == -7) return 0.0009765625*sqrt(170170/M_PI)*yy*zz*(672.0*xx6 - 3360.0*xx4*yy2 + 2016.0*xx2*yy4 - 96.0*yy6 - 798.0*xx8 + 3192.0*xx6*yy2 + 1596.0*xx4*yy4 - 2280.0*xx2*yy6 + 114.0*yy8);
		if(mm == -6) return 0.00048828125*sqrt(10010/M_PI)*xx*yy*(8064.0*xx4 - 26880.0*xx2*yy2 + 8064.0*yy4 - 19584.0*xx6 + 45696.0*xx4*yy2 + 45696.0*xx2*yy4
																															- 19584.0*yy6 + 11628.0*xx8 - 15504.0*xx6*yy2 - 54264.0*xx4*yy4 - 15504.0*xx2*yy6 + 11628.0*yy8);
		if(mm == -5) return 0.001953125*sqrt(2002/M_PI)*yy*zz*(5040.0*xx4 - 10080.0*xx2*yy2 + 1008.0*yy4 - 14280.0*xx6 + 14280.0*xx4*yy2
																													 + 25704.0*xx2*yy4 - 2856.0*yy6 + 9690.0*xx8 - 27132.0*xx4*yy4 - 15504.0*xx2*yy6 + 1938.0*yy8);
		if(mm == -4) return 0.001953125*sqrt(5005/M_PI)*xx*yy*(2688.0*xx2 - 2688.0*yy2 - 12096.0*xx4 + 12096.0*yy4 + 17136.0*xx6 + 17136.0*xx4*yy2 - 17136.0*xx2*yy4
																													 - 17136.0*yy6 - 7752.0*xx8 - 15504.0*xx6*yy2 + 15504.0*xx2*yy6 + 7752.0*yy8);
		if(mm == -3) return 0.001953125*sqrt(10010/M_PI)*yy*zz*(1152.0*xx2 - 384.0*yy2 - 6480.0*xx4 - 4320.0*xx2*yy2 + 2160.0*yy4 + 11016.0*xx6 + 18360.0*xx4*yy2
																														+ 3672.0*xx2*yy4 - 3672.0*yy6 - 5814.0*xx8 - 15504.0*xx6*yy2 - 11628.0*xx4*yy4 + 1938.0*yy8);
		if(mm == -2) return 0.0009765625*sqrt(385/M_PI)*xx*yy*(4608.0 - 39936.0*xx2 - 39936.0*yy2 + 112320.0*xx4 + 224640.0*xx2*yy2 + 112320.0*yy4 - 127296.0*xx6
																													 - 381888.0*xx4*yy2 - 381888.0*xx2*yy4 - 127296.0*yy6 + 50388.0*xx8 + 201552.0*xx6*yy2
																													 + 302328.0*xx4*yy4 + 201552.0*xx2*yy6 + 50388.0*yy8);
		if(mm == -1) return 0.00390625*sqrt(1155/M_PI)*yy*zz*(128 - 1664*xx2 - 1664*yy2 + 6240*xx4 + 12480*xx2*yy2 + 6240*yy4 - 8840*xx6 - 26520*xx4*yy2
																													- 26520*xx2*yy4 - 8840*yy6 + 4199*xx8 + 16796*xx6*yy2 + 25194*xx4*yy4 + 16796*xx2*yy6 + 4199*yy8);
		if(mm ==  0) return 1.0/512.0*sqrt(21/M_PI)*(256 - 7040*xx2 - 7040*yy2 + 45760*xx4 + 91520*xx2*yy2 + 45760*yy4 - 114400*xx6 - 343200*xx4*yy2 - 343200*xx2*yy4 - 114400*yy6
																								 + 121550*xx8 + 486200*xx6*yy2 + 729300*xx4*yy4 + 486200*xx2*yy6 + 121550*yy8 - 46189*xx8*xx2 - 230945*xx8*yy2
																								 - 461890*xx6*yy4 - 461890*xx4*yy6 - 230945*xx2*yy8 - 46189*yy4*yy6);
		if(mm ==  1) return 1.0/256.0*sqrt(1155/M_PI)*xx*zz*(128 - 1664*xx2 - 1664*yy2 + 6240*xx4 + 12480*xx2*yy2 + 6240*yy4 - 8840*xx6 - 26520*xx4*yy2 - 26520*xx2*yy4
																												 - 8840*yy6 + 4199*xx8 + 16796*xx6*yy2 + 25194*xx4*yy4 + 16796*xx2*yy6 + 4199*yy8);
		if(mm ==  2) return 3.0/512.0*sqrt(385/M_PI)*(xx - yy)*(xx + yy)*(384 - 3328*xx2 - 3328*yy2 + 9360*xx4 + 18720*xx2*yy2 + 9360*yy4 - 10608*xx6 - 31824*xx4*yy2 - 31824*xx2*yy4
																																			- 10608*yy6 + 4199*xx8 + 16796*xx6*yy2 + 25194*xx4*yy4 + 16796*xx2*yy6 + 4199*yy8);
		if(mm ==  3) return -3.0/256.0*sqrt(10010/M_PI)*xx*zz*(xx2 - 3*yy2)*(-64 + 360*xx2 + 360*yy2 - 612*xx4 - 1224*xx2*yy2 - 612*yy4 + 323*xx6 + 969*xx4*yy2 + 969*xx2*yy4 + 323*yy6);
		if(mm ==  4) return 3.0/256.0*sqrt(5005/M_PI)*(-xx2 + 2*xx*yy + yy2)*(xx2 + 2*xx*yy - yy2)*(-112 + 504*xx2 + 504*yy2 - 714*xx4 - 1428*xx2*yy2 - 714*yy4 + 323*xx6
																																																+ 969*xx4*yy2 + 969*xx2*yy4 + 323*yy6);
		if(mm ==  5) return 3.0/256.0*sqrt(2002/M_PI)*xx*zz*(xx4 - 10*xx2*yy2 + 5*yy4)*(168 - 476*xx2 - 476*yy2 + 323*xx4 + 646*xx2*yy2 + 323*yy4);
		if(mm ==  6) return 3.0/1024.0*sqrt(10010/M_PI)*(xx - yy)*(xx + yy)*(xx2 - 4*xx*yy + yy2)*(xx2 + 4*xx*yy + yy2)*(224 - 544*xx2 - 544*yy2 + 323*xx4 + 646*xx2*yy2 + 323*yy4);
		if(mm ==  7) return -3.0/512.0*sqrt(170170/M_PI)*xx*zz*(-16 + 19*xx2 + 19*yy2)*(xx6 - 21*xx4*yy2 + 35*xx2*yy4 - 7*yy6);
		if(mm ==  8) return -1.0/512.0*sqrt(255255/M_PI)*(-18 + 19*xx2 + 19*yy2)*(xx4 - 4*xx2*xx*yy - 6*xx2*yy2 + 4*xx*yy2*yy + yy4)*(xx4 + 4*xx2*xx*yy - 6*xx2*yy2 - 4*xx*yy2*yy + yy4);
		if(mm ==  9) return 1.0/512.0*sqrt(9699690/M_PI)*xx*zz*(xx2 - 3*yy2)*(xx6 - 33*xx4*yy2 + 27*xx2*yy4 - 3*yy6);
		if(mm ==  10) return 1.0/1024.0*sqrt(1939938/M_PI)*(xx6*xx4 - 45*xx8*yy2 + 210*xx6*yy4 - 210*xx4*yy6 + 45*xx2*yy8 - yy4*yy6);
		return 0.0;
	default:
		return 0.0;
		/*
		// This is the general implementation that doesn't work on the GPU and it is slower
		
			auto sh = std::sph_legendre(ll, abs(mm), acos(zz));
			auto phi = atan2(yy, xx);
			if(mm > 0){
			sh *= pow(-1, mm)*std::sqrt(8.0*M_PI)*cos(mm*phi);
			} else if (mm == 0) {
			sh *= std::sqrt(4.0*M_PI);
			} else {
			sh *= pow(-1, mm)*std::sqrt(8.0*M_PI)*sin(-mm*phi);
			}
			return 1.0/sqrt(4.0*M_PI)*sh;
		*/
	}
}

GPU_FUNCTION auto sharmonic_real(int const ll, int const mm, double xx, double yy, double zz) {

	if(ll == 0) return 1.0/sqrt(4.0*M_PI);
	auto norm = normalize(xx, yy, zz);
	if(norm < 1.0e-15) return 0.0;

	return sharmonic_real_normalized(ll, mm, xx, yy, zz);
}

template <typename VectorT>
GPU_FUNCTION auto sharmonic_real(int const ll, int const mm, VectorT vector) {
	return sharmonic_real(ll, mm, vector[0], vector[1], vector[2]);
}

//this functions converts an mm > 0 harmonic into the -mm harmonic
template <typename Complex>
GPU_FUNCTION Complex neg(int mm, Complex sph) {
	if(mm >= 0) return sph;
	if(abs(mm)%2 == 0) return conj(sph);
	return -conj(sph);
}

template <typename Complex = std::complex<double>>
GPU_FUNCTION Complex sharmonic_complex(int const ll, int const mm, typename Complex::value_type xx, typename Complex::value_type yy, typename Complex::value_type zz) {

	if(ll == 0) return 1.0/sqrt(4.0*M_PI);

	auto norm = normalize(xx, yy, zz);
	if(norm < 1.0e-15) return 0.0;

	if(mm == 0) return sharmonic_real_normalized(ll, mm, xx, yy, zz);
	return neg(mm, pow(-1.0, abs(mm))*0.5*sqrt(2.0)*Complex(sharmonic_real_normalized(ll, abs(mm), xx, yy, zz), sharmonic_real_normalized(ll, -abs(mm), xx, yy, zz)));
}

template <typename Complex, typename VectorT>
GPU_FUNCTION auto sharmonic_complex(int const ll, int const mm, VectorT vector) {
	return sharmonic_complex<Complex>(ll, mm, vector[0], vector[1], vector[2]);
}

}
}

#endif

#ifdef INQ_MATH_SHARMONIC_UNIT_TEST
#undef INQ_MATH_SHARMONIC_UNIT_TEST

#include <catch2/catch_all.hpp>

#ifdef ENABLE_GPU
#include <thrust/complex.h>
using complex = thrust::complex<double>;
#else
#include <complex>
using complex = std::complex<double>;
#endif

GPU_FUNCTION inline double real(const complex & z){
	return z.real();
}

GPU_FUNCTION inline auto imag(const complex & z){
	return z.imag();
}

#ifdef GENERATE_REFERENCE
complex bsh(int ll, int mm, double xx, double yy, double zz){

	if(ll == 0) return 1.0/sqrt(4.0*M_PI);
	auto norm = pseudo::math::normalize(xx, yy, zz);
	if(norm < 1.0e-15) return 0.0;

	// this is the reference implementation using boost.
	auto phi = atan2(yy, xx);
	if(phi < 0.0) phi += 2.0*M_PI;
	auto theta = acos(zz);
	return boost::math::spherical_harmonic(ll, mm, theta, phi);
}

double real_bsh(int ll, int mm, double xx, double yy, double zz){
	auto sh = bsh(ll, abs(mm), xx, yy, zz);
	if(mm < 0) return sqrt(2.0)*pow(-1.0, mm)*imag(sh);
	if(mm > 0) return sqrt(2.0)*pow(-1.0, mm)*real(sh);
	return real(sh);
}

#endif

TEST_CASE(INQ_TEST_FILE, INQ_TEST_TAG) {
	using namespace Catch::literals;
	using Catch::Approx;

	using pseudo::math::normalize;
	using pseudo::math::sharmonic_real;
	using pseudo::math::sharmonic_complex;

#ifdef GENERATE_REFERENCE
	SECTION("generate reference values"){

		auto file = std::ofstream("spherical_harmonic_reference.dat");

		auto lmax = 10;
		auto reps = 100;

		int count = 0;
		for(int ll = 0; ll <= lmax; ll++) count += reps*(2*ll + 1);

		file << count << std::endl;
		file << "# l m x y z re_complex im_complex real" << std::endl;
		
		for(int ll = 0; ll <= lmax; ll++) {
			for(int mm = -ll; mm <= ll; mm++) {
				for(int iter = 0; iter < reps; iter++){
					double xx = 2.0*(drand48() - 0.5);
					double yy = 2.0*(drand48() - 0.5);
					double zz = 2.0*(drand48() - 0.5);

					if(iter == 0) {
						xx = 0.0;
						yy = 0.0;
						zz = 0.0;
					}
					
					auto ref_sh = bsh(ll, mm, xx, yy, zz);
					auto real_sh = real_bsh(ll, mm, xx, yy, zz);

					if(ll <= SHARMONIC_MAX_L) {

						auto sh = sharmonic_complex<complex>(ll, mm, xx, yy, zz);

						auto diff = fabs(sh - ref_sh);
						if(diff > 1e-13) std::cout << ll << '\t' << mm << '\t' << xx << '\t' << yy << '\t' << zz << '\t' << sh << '\t' << ref_sh << '\t' << diff << '\n';
						
						CHECK(sh.real() == Approx(ref_sh.real()));
						CHECK(sh.imag() == Approx(ref_sh.imag()));

						auto rsh = sharmonic_real(ll, mm, xx, yy, zz);
						CHECK(rsh == Approx(real_sh));
					}
					
					file << std::scientific << std::setprecision(20) << ll << '\t' << mm << '\t' << xx << '\t' << yy << '\t' << zz << '\t' << ref_sh.real() << '\t' << ref_sh.imag() << '\t' << real_sh << '\n';
				}
			}
		}
	}
#endif
	
	SECTION("reference values complex") {

		std::ifstream file(pseudopod::path::unit_tests_data() + "spherical_harmonic_reference.dat");

		REQUIRE(file.is_open());
		
		int count;
		std::string comment_line;
		
		file >> count;

		std::getline(file, comment_line);
		std::getline(file, comment_line);
		
		int ll, mm;
		double xx, yy, zz, re, im, realh;
		
		for(int ii = 0; ii < count; ii++){
			file >> ll >> mm >> xx >> yy >> zz >> re >> im >> realh;

			if(ll <= SHARMONIC_MAX_L) {
				CHECK(real(sharmonic_complex<complex>(ll, mm, xx, yy, zz)) == Approx(re));
				CHECK(imag(sharmonic_complex<complex>(ll, mm, xx, yy, zz)) == Approx(im));
				CHECK(sharmonic_real(ll, mm, xx, yy, zz) == Approx(realh));
			}
		}
		
	}	
	
	SECTION("normalize") {

		{
			auto xx =  3.0;
			auto yy = -5.0;
			auto zz =  4.0;
			CHECK(normalize(xx, yy, zz) == 7.0710678119_a);
			CHECK(normalize(xx, yy, zz) == 1.0_a);
		}

		{
			auto xx =  3.0e-15;
			auto yy = -5.0e-15;
			auto zz =  4.0e-15;
			CHECK(normalize(xx, yy, zz) == 7.0710678119e-15_a);
			CHECK(normalize(xx, yy, zz) == 1.0_a);
		}
		
		{
			auto xx =  3.0e-20;
			auto yy = -5.0e-20;
			auto zz =  4.0e-20;
			CHECK(normalize(xx, yy, zz) == 7.0710678119e-20_a);
			CHECK(normalize(xx, yy, zz) == 1.0_a);
		}
		
		{
			auto xx =  0.0;
			auto yy =  0.0;
			auto zz =  0.0;
			CHECK(normalize(xx, yy, zz) == 0.0_a);
		}
		
	}
	
	SECTION("l=0"){
		CHECK(sharmonic_real(0, 0, 0.0, 0.0, 0.0) == 0.2820947918_a);

		CHECK(sharmonic_real(0, 0, 1.0, 0.0, 0.0) == 0.2820947918_a);
		CHECK(sharmonic_real(0, 0, 0.0, 1.0, 0.0) == 0.2820947918_a);
		CHECK(sharmonic_real(0, 0, 0.0, 0.0, 1.0) == 0.2820947918_a);
	}

	SECTION("l=1"){
		CHECK(sharmonic_real(1, -1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1, -1, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1, -1, 0.0, 3.0, 0.0) == 0.4886025119_a);
		CHECK(sharmonic_real(1, -1, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1, -1, 0.5, 1.5, 2.5) == 0.2477666951_a);
		
		CHECK(sharmonic_real(1,	0, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1,	0, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1,	0, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1,	0, 0.0, 0.0, 2.0) == 0.4886025119_a);
		CHECK(sharmonic_real(1,	0, 0.5, 1.5, 2.5) == 0.4129444918_a);
		
		CHECK(sharmonic_real(1,	1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1,	1, 2.0, 0.0, 0.0) == 0.4886025119_a);
		CHECK(sharmonic_real(1,	1, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1,	1, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(1,	1, 0.5, 1.5, 2.5) == 0.0825888984_a);
	}
	
	SECTION("l=2"){
		CHECK(sharmonic_real(2, -2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -2, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -2, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -2, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -2, 0.5, 1.5, 2.5) == 0.0936470083_a);
		
		CHECK(sharmonic_real(2, -1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -1, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -1, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -1, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2, -1, 0.5, 1.5, 2.5) == 0.4682350417_a);
		
		CHECK(sharmonic_real(2,	0, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2,	0, 2.0, 0.0, 0.0) == -0.3153915653_a);
		CHECK(sharmonic_real(2,	0, 0.0, 2.0, 0.0) == -0.3153915653_a);
		CHECK(sharmonic_real(2,	0, 0.0, 0.0, 2.0) == 0.6307831305_a);
		CHECK(sharmonic_real(2,	0, 0.5, 1.5, 2.5) == 0.3604475031_a);
		
		CHECK(sharmonic_real(2,	1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2,	1, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2,	1, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2,	1, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2,	1, 0.5, 1.5, 2.5) == 0.1560783472_a);

		CHECK(sharmonic_real(2,	2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2,	2, 2.0, 0.0, 0.0) == 0.5462742153_a);
		CHECK(sharmonic_real(2,	2, 0.0, 2.0, 0.0) == -0.5462742153_a);
		CHECK(sharmonic_real(2,	2, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(2,	2, 0.5, 1.5, 2.5) == -0.1248626778_a);
	}

	SECTION("l=3"){
		CHECK(sharmonic_real(3, -3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -3, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -3, 0.0, 3.0, 0.0) == -0.5900435899_a);
		CHECK(sharmonic_real(3, -3, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -3, 0.5, 1.5, 2.5) == -0.0512925789_a);
		
		CHECK(sharmonic_real(3, -2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -2, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -2, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -2, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -2, 0.5, 1.5, 2.5) == 0.2094010765_a);
		
		CHECK(sharmonic_real(3, -1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -1, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -1, 0.0, 3.0, 0.0) == -0.4570457995_a);
		CHECK(sharmonic_real(3, -1, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3, -1, 0.5, 1.5, 2.5) == 0.5959659117_a);
		
		CHECK(sharmonic_real(3,	0, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	0, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	0, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	0, 0.0, 0.0, 2.0) == 0.7463526652_a);
		CHECK(sharmonic_real(3,	0, 0.5, 1.5, 2.5) == 0.1802237516_a);
		
		CHECK(sharmonic_real(3,	1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	1, 2.0, 0.0, 0.0) == -0.4570457995_a);
		CHECK(sharmonic_real(3,	1, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	1, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	1, 0.5, 1.5, 2.5) == 0.1986553039_a);

		CHECK(sharmonic_real(3,	2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	2, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	2, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	2, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	2, 0.5, 1.5, 2.5) == -0.2792014354_a);

		CHECK(sharmonic_real(3,	3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	3, 2.0, 0.0, 0.0) == 0.5900435899_a);
		CHECK(sharmonic_real(3,	3, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	3, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(3,	3, 0.5, 1.5, 2.5) == -0.0740892806_a);
	}

	SECTION("l=4"){
		CHECK(sharmonic_real(4, -4, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -4, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -4, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -4, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -4, 0.5, 1.5, 2.5) == -0.0490450862_a);
		
		CHECK(sharmonic_real(4, -3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -3, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -3, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -3, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -3, 0.5, 1.5, 2.5) == -0.1300504239_a);
		
		CHECK(sharmonic_real(4, -2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -2, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -2, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -2, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -2, 0.5, 1.5, 2.5) == 0.3244027528_a);
		
		CHECK(sharmonic_real(4, -1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -1, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -1, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -1, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4, -1, 0.5, 1.5, 2.5) == 0.5734684659_a);
		
		CHECK(sharmonic_real(4,	0, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	0, 2.0, 0.0, 0.0) == 0.3173566407_a);
		CHECK(sharmonic_real(4,	0, 0.0, 2.0, 0.0) == 0.3173566407_a);
		CHECK(sharmonic_real(4,	0, 0.0, 0.0, 2.0) == 0.8462843753_a);
		CHECK(sharmonic_real(4,	0, 0.5, 1.5, 2.5) == -0.060448884_a);
		
		CHECK(sharmonic_real(4,	1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	1, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	1, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	1, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	1, 0.5, 1.5, 2.5) == 0.1911561553_a);

		CHECK(sharmonic_real(4,	2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	2, 2.0, 0.0, 0.0) == -0.4730873479_a);
		CHECK(sharmonic_real(4,	2, 0.0, 2.0, 0.0) == 0.4730873479_a);
		CHECK(sharmonic_real(4,	2, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	2, 0.5, 1.5, 2.5) == -0.4325370038_a);

		CHECK(sharmonic_real(4,	3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	3, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	3, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	3, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	3, 0.5, 1.5, 2.5) == -0.1878506123_a);
		
		CHECK(sharmonic_real(4,	4, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	4, 3.0, 0.0, 0.0) == 0.6258357354_a);
		CHECK(sharmonic_real(4,	4, 0.0, 3.0, 0.0) == 0.6258357354_a);
		CHECK(sharmonic_real(4,	4, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(4,	4, 0.5, 1.5, 2.5) == 0.0143048168_a);
	}
	
	SECTION("l=5"){
		CHECK(sharmonic_real(5, -5, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -5, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -5, 0.0, 3.0, 0.0) == 0.6563820568_a);
		CHECK(sharmonic_real(5, -5, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -5, 0.5, 1.5, 2.5) == -0.0010868456_a);
		
		CHECK(sharmonic_real(5, -4, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -4, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -4, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -4, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -4, 0.5, 1.5, 2.5) == -0.1374762974_a);
		
		CHECK(sharmonic_real(5, -3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -3, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -3, 0.0, 3.0, 0.0) == 0.4892382994_a);
		CHECK(sharmonic_real(5, -3, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -3, 0.5, 1.5, 2.5) == -0.2308747526_a);
		
		CHECK(sharmonic_real(5, -2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -2, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -2, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -2, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -2, 0.5, 1.5, 2.5) == 0.3968598866_a);
		
		CHECK(sharmonic_real(5, -1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -1, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -1, 0.0, 3.0, 0.0) == 0.4529466512_a);
		CHECK(sharmonic_real(5, -1, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5, -1, 0.5, 1.5, 2.5) == 0.393747212_a);
		
		CHECK(sharmonic_real(5,	0, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	0, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	0, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	0, 0.0, 0.0, 2.0) == 0.9356025796_a);
		CHECK(sharmonic_real(5,	0, 0.5, 1.5, 2.5) == -0.282403036_a);
		
		CHECK(sharmonic_real(5,	1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	1, 2.0, 0.0, 0.0) == 0.4529466512_a);
		CHECK(sharmonic_real(5,	1, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	1, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	1, 0.5, 1.5, 2.5) == 0.1312490707_a);

		CHECK(sharmonic_real(5,	2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	2, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	2, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	2, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	2, 0.5, 1.5, 2.5) == -0.5291465155_a);

		CHECK(sharmonic_real(5,	3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	3, 2.0, 0.0, 0.0) == -0.4892382994_a);
		CHECK(sharmonic_real(5,	3, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	3, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	3, 0.5, 1.5, 2.5) == -0.3334857538_a);
		
		CHECK(sharmonic_real(5,	4, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	4, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	4, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	4, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	4, 0.5, 1.5, 2.5) == 0.0400972534_a);

		CHECK(sharmonic_real(5,	5, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	5, 3.0, 0.0, 0.0) == 0.6563820568_a);
		CHECK(sharmonic_real(5,	5, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	5, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(5,	5, 0.5, 1.5, 2.5) == 0.0286202664_a);
	}
	
	SECTION("l=6"){
		CHECK(sharmonic_real(6, -6, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -6, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -6, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -6, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -6, 0.5, 1.5, 2.5) == 0.0149145265_a);
		
		CHECK(sharmonic_real(6, -5, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -5, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -5, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -5, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -5, 0.5, 1.5, 2.5) == -0.0033118869_a);
		
		CHECK(sharmonic_real(6, -4, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -4, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -4, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -4, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -4, 0.5, 1.5, 2.5) == -0.2711411152_a);
		
		CHECK(sharmonic_real(6, -3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -3, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -3, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -3, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -3, 0.5, 1.5, 2.5) == -0.3287333054_a);
		
		CHECK(sharmonic_real(6, -2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -2, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -2, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -2, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -2, 0.5, 1.5, 2.5) == 0.3931908163_a);
		
		CHECK(sharmonic_real(6, -1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -1, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -1, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -1, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6, -1, 0.5, 1.5, 2.5) == 0.1019162733_a);
		
		CHECK(sharmonic_real(6,	0, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	0, 2.0, 0.0, 0.0) == -0.3178460113_a);
		CHECK(sharmonic_real(6,	0, 0.0, 2.0, 0.0) == -0.3178460113_a);
		CHECK(sharmonic_real(6,	0, 0.0, 0.0, 2.0) == 1.0171072363_a);
		CHECK(sharmonic_real(6,	0, 0.5, 1.5, 2.5) == -0.4151458107_a);
		
		CHECK(sharmonic_real(6,	1, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	1, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	1, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	1, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	1, 0.5, 1.5, 2.5) == 0.0339720911_a);

		CHECK(sharmonic_real(6,	2, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	2, 2.0, 0.0, 0.0) == 0.4606026298_a);
		CHECK(sharmonic_real(6,	2, 0.0, 2.0, 0.0) == -0.4606026298_a);
		CHECK(sharmonic_real(6,	2, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	2, 0.5, 1.5, 2.5) == -0.5242544217_a);

		CHECK(sharmonic_real(6,	3, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	3, 2.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	3, 0.0, 2.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	3, 0.0, 0.0, 2.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	3, 0.5, 1.5, 2.5) == -0.4748369967_a);
		
		CHECK(sharmonic_real(6,	4, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	4, 3.0, 0.0, 0.0) == -0.5045649007_a);
		CHECK(sharmonic_real(6,	4, 0.0, 3.0, 0.0) == -0.5045649007_a);
		CHECK(sharmonic_real(6,	4, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	4, 0.5, 1.5, 2.5) == 0.0790828253_a);

		CHECK(sharmonic_real(6,	5, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	5, 3.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	5, 0.0, 3.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	5, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	5, 0.5, 1.5, 2.5) == 0.087213021_a);

		CHECK(sharmonic_real(6,	6, 0.0, 0.0, 0.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	6, 3.0, 0.0, 0.0) == 0.6831841052_a);
		CHECK(sharmonic_real(6,	6, 0.0, 3.0, 0.0) == -0.6831841052_a);
		CHECK(sharmonic_real(6,	6, 0.0, 0.0, 3.0) == (0.0_a).margin(1e-12));
		CHECK(sharmonic_real(6,	6, 0.5, 1.5, 2.5) == 0.0056088817_a);
	}

	SECTION("complex l=0"){
		CHECK(real(sharmonic_complex<complex>(0, 0, 0.0, 0.0, 0.0)) == 0.2820947918_a);	
		
		CHECK(real(sharmonic_complex<complex>(0, 0, 1.0, 0.0, 0.0)) == 0.2820947918_a);
		CHECK(real(sharmonic_complex<complex>(0, 0, 0.0, 1.0, 0.0)) == 0.2820947918_a);
		CHECK(real(sharmonic_complex<complex>(0, 0, 0.0, 0.0, 1.0)) == 0.2820947918_a);

		
		CHECK(imag(sharmonic_complex<complex>(0, 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));

		CHECK(imag(sharmonic_complex<complex>(0, 0, 1.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(0, 0, 0.0, 1.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(0, 0, 0.0, 0.0, 1.0)) == (0.0_a).margin(1e-12));

	}

	SECTION("complex l=1"){

#ifdef GENERATE_REFERENCE
		CHECK(real(bsh(1, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(bsh(1, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
#endif
		
		CHECK(real(sharmonic_complex<complex>(1, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1, -1, 3.0, 0.0, 0.0)) == 0.3454941495_a);
		CHECK(real(sharmonic_complex<complex>(1, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1, -1, 0.5, 1.5, 2.5)) == 0.0583991701_a);

		CHECK(imag(sharmonic_complex<complex>(1, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1, -1, 0.0, 3.0, 0.0)) == -0.3454941495_a);
		CHECK(imag(sharmonic_complex<complex>(1, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1, -1, 0.5, 1.5, 2.5)) == -0.1751975102_a);
		
		CHECK(real(sharmonic_complex<complex>(1,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1,	 0, 0.0, 0.0, 2.0)) == 0.4886025119_a);
		CHECK(real(sharmonic_complex<complex>(1,	 0, 0.5, 1.5, 2.5)) == 0.4129444918_a);

		CHECK(imag(sharmonic_complex<complex>(1,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1,	 0, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1,	 0, 0.5, 1.5, 2.5)) == (0.0_a).margin(1e-12));

		CHECK(real(sharmonic_complex<complex>(1,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1,	 1, 2.0, 0.0, 0.0)) == -0.3454941495_a);
		CHECK(real(sharmonic_complex<complex>(1,	 1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(1,	 1, 0.5, 1.5, 2.5)) == -0.0583991701_a);

		CHECK(imag(sharmonic_complex<complex>(1,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1,	 1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1,	 1, 0.0, 2.0, 0.0)) == -0.3454941495_a);
		CHECK(imag(sharmonic_complex<complex>(1,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(1,	 1, 0.5, 1.5, 2.5)) == -0.1751975102_a);
	}

	SECTION("complex l=2"){

		CHECK(real(sharmonic_complex<complex>(2, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2, -2, 3.0, 0.0, 0.0)) ==	0.386274202_a);
		CHECK(real(sharmonic_complex<complex>(2, -2, 0.0, 3.0, 0.0)) == -0.386274202_a);
		CHECK(real(sharmonic_complex<complex>(2, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2, -2, 0.5, 1.5, 2.5)) == -0.0882912462_a);

		CHECK(imag(sharmonic_complex<complex>(2, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -2, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -2, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -2, 0.5, 1.5, 2.5)) == -0.0662184346_a);

		CHECK(real(sharmonic_complex<complex>(2, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2, -1, 0.5, 1.5, 2.5)) == 0.1103640577_a);

		CHECK(imag(sharmonic_complex<complex>(2, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2, -1, 0.5, 1.5, 2.5)) == -0.3310921732_a);

		CHECK(real(sharmonic_complex<complex>(2,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2,	 0, 2.0, 0.0, 0.0)) == -0.3153915653_a);
		CHECK(real(sharmonic_complex<complex>(2,	 0, 0.0, 2.0, 0.0)) == -0.3153915653_a);
		CHECK(real(sharmonic_complex<complex>(2,	 0, 0.0, 0.0, 2.0)) == 0.6307831305_a);
		CHECK(real(sharmonic_complex<complex>(2,	 0, 0.5, 1.5, 2.5)) == 0.3604475031_a);

		CHECK(imag(sharmonic_complex<complex>(2,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 0, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 0, 0.5, 1.5, 2.5)) == (0.0_a).margin(1e-12));

		CHECK(real(sharmonic_complex<complex>(2,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2,	 1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2,	 1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2,	 1, 0.5, 1.5, 2.5)) == -0.1103640577_a);

		CHECK(imag(sharmonic_complex<complex>(2,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 1, 0.5, 1.5, 2.5)) == -0.3310921732_a);

		CHECK(real(sharmonic_complex<complex>(2,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2,	 2, 2.0, 0.0, 0.0)) == 0.386274202_a);
		CHECK(real(sharmonic_complex<complex>(2,	 2, 0.0, 2.0, 0.0)) == -0.386274202_a);
		CHECK(real(sharmonic_complex<complex>(2,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(2,	 2, 0.5, 1.5, 2.5)) == -0.0882912462_a);
		
		CHECK(imag(sharmonic_complex<complex>(2,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 2, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 2, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(2,	 2, 0.5, 1.5, 2.5)) == 0.0662184346_a);
		
	}

	SECTION("complex l=3"){
		
		CHECK(real(sharmonic_complex<complex>(3, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -3, 3.0, 0.0, 0.0)) == 0.4172238236_a);
		CHECK(real(sharmonic_complex<complex>(3, -3, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -3, 0.5, 1.5, 2.5)) == -0.0523890328_a);
		
		CHECK(imag(sharmonic_complex<complex>(3, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -3, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -3, 0.0, 3.0, 0.0)) == 0.4172238236_a);
		CHECK(imag(sharmonic_complex<complex>(3, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -3, 0.5, 1.5, 2.5)) == 0.0362693304_a);
		
		CHECK(real(sharmonic_complex<complex>(3, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -2, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -2, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -2, 0.5, 1.5, 2.5)) == -0.1974252283_a);

		CHECK(imag(sharmonic_complex<complex>(3, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -2, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -2, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -2, 0.5, 1.5, 2.5)) == -0.1480689212_a);
		
		CHECK(real(sharmonic_complex<complex>(3, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -1, 3.0, 0.0, 0.0)) == -0.3231801841_a);
		CHECK(real(sharmonic_complex<complex>(3, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3, -1, 0.5, 1.5, 2.5)) == 0.1404705125_a);

		CHECK(imag(sharmonic_complex<complex>(3, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -1, 0.0, 3.0, 0.0)) == 0.3231801841_a);
		CHECK(imag(sharmonic_complex<complex>(3, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3, -1, 0.5, 1.5, 2.5)) == -0.4214115375_a);
		
		CHECK(real(sharmonic_complex<complex>(3,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 0, 0.0, 0.0, 2.0)) == 0.7463526652_a);
		CHECK(real(sharmonic_complex<complex>(3,	 0, 0.5, 1.5, 2.5)) == 0.1802237516_a);

		CHECK(imag(sharmonic_complex<complex>(3,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 0, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 0, 0.5, 1.5, 2.5)) == (0.0_a).margin(1e-12));
		
		CHECK(real(sharmonic_complex<complex>(3,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 1, 2.0, 0.0, 0.0)) == 0.3231801841_a);
		CHECK(real(sharmonic_complex<complex>(3,	 1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 1, 0.5, 1.5, 2.5)) == -0.1404705125_a);

		CHECK(real(sharmonic_complex<complex>(3,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 2, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 2, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 2, 0.5, 1.5, 2.5)) == -0.1974252283_a);

		CHECK(imag(sharmonic_complex<complex>(3,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 2, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 2, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 2, 0.5, 1.5, 2.5)) == 0.1480689212_a);
		
		CHECK(real(sharmonic_complex<complex>(3,	 3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 3, 2.0, 0.0, 0.0)) == -0.4172238236_a);
		CHECK(real(sharmonic_complex<complex>(3,	 3, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(3,	 3, 0.5, 1.5, 2.5)) == 0.0523890328_a);

		CHECK(imag(sharmonic_complex<complex>(3,	 3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 3, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 3, 0.0, 2.0, 0.0)) == 0.4172238236_a);
		CHECK(imag(sharmonic_complex<complex>(3,	 3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(3,	 3, 0.5, 1.5, 2.5)) == 0.0362693304_a);		
	}

	SECTION("complex l=4"){
		
		CHECK(real(sharmonic_complex<complex>(4, -4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -4, 3.0, 0.0, 0.0)) == 0.4425326924_a);
		CHECK(real(sharmonic_complex<complex>(4, -4, 0.0, 3.0, 0.0)) == 0.4425326924_a);
		CHECK(real(sharmonic_complex<complex>(4, -4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -4, 0.5, 1.5, 2.5)) == 0.010115033_a);

		CHECK(real(sharmonic_complex<complex>(4, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -3, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -3, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -3, 0.5, 1.5, 2.5)) == -0.1328304418_a);
		
		CHECK(real(sharmonic_complex<complex>(4, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -2, 3.0, 0.0, 0.0)) == -0.3345232718_a);
		CHECK(real(sharmonic_complex<complex>(4, -2, 0.0, 3.0, 0.0)) == 0.3345232718_a);
		CHECK(real(sharmonic_complex<complex>(4, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -2, 0.5, 1.5, 2.5)) == -0.3058498485_a);
		
		CHECK(real(sharmonic_complex<complex>(4, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4, -1, 0.5, 1.5, 2.5)) == 0.1351678137_a);
		
		CHECK(real(sharmonic_complex<complex>(4,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 0, 2.0, 0.0, 0.0)) == 0.3173566407_a);
		CHECK(real(sharmonic_complex<complex>(4,	 0, 0.0, 2.0, 0.0)) == 0.3173566407_a);
		CHECK(real(sharmonic_complex<complex>(4,	 0, 0.0, 0.0, 2.0)) == 0.8462843753_a);
		CHECK(real(sharmonic_complex<complex>(4,	 0, 0.5, 1.5, 2.5)) == -0.060448884_a);
		
		CHECK(real(sharmonic_complex<complex>(4,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 1, 0.5, 1.5, 2.5)) == -0.1351678137_a);

		CHECK(real(sharmonic_complex<complex>(4,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 2, 2.0, 0.0, 0.0)) == -0.3345232718_a);
		CHECK(real(sharmonic_complex<complex>(4,	 2, 0.0, 2.0, 0.0)) == 0.3345232718_a);
		CHECK(real(sharmonic_complex<complex>(4,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 2, 0.5, 1.5, 2.5)) == -0.3058498485_a);

		CHECK(real(sharmonic_complex<complex>(4,	 3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 3, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 3, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 3, 0.5, 1.5, 2.5)) == 0.1328304418_a);
		
		CHECK(real(sharmonic_complex<complex>(4,	 4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 4, 3.0, 0.0, 0.0)) == 0.4425326924_a);
		CHECK(real(sharmonic_complex<complex>(4,	 4, 0.0, 3.0, 0.0)) == 0.4425326924_a);
		CHECK(real(sharmonic_complex<complex>(4,	 4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(4,	 4, 0.5, 1.5, 2.5)) == 0.010115033_a);

		CHECK(imag(sharmonic_complex<complex>(4, -4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -4, 0.5, 1.5, 2.5)) == 0.034680113_a);

		CHECK(imag(sharmonic_complex<complex>(4, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -3, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -3, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -3, 0.5, 1.5, 2.5)) == 0.0919595366_a);
		
		CHECK(imag(sharmonic_complex<complex>(4, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -2, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -2, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -2, 0.5, 1.5, 2.5)) == -0.2293873864_a);
		
		CHECK(imag(sharmonic_complex<complex>(4, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4, -1, 0.5, 1.5, 2.5)) == -0.405503441_a);
		
		CHECK(imag(sharmonic_complex<complex>(4,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 0, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 0, 0.5, 1.5, 2.5)) == (0.0_a).margin(1e-12));
		
		CHECK(imag(sharmonic_complex<complex>(4,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 1, 0.5, 1.5, 2.5)) == -0.405503441_a);

		CHECK(imag(sharmonic_complex<complex>(4,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 2, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 2, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 2, 0.5, 1.5, 2.5)) == 0.2293873864_a);

		CHECK(imag(sharmonic_complex<complex>(4,	 3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 3, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 3, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 3, 0.5, 1.5, 2.5)) == 0.0919595366_a);
		
		CHECK(imag(sharmonic_complex<complex>(4,	 4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(4,	 4, 0.5, 1.5, 2.5)) == -0.034680113_a);
		
	}
	
	SECTION("complex l=5"){
		
		CHECK(real(sharmonic_complex<complex>(5, -5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -5, 3.0, 0.0, 0.0)) == 0.4641322034_a);
		CHECK(real(sharmonic_complex<complex>(5, -5, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -5, 0.5, 1.5, 2.5)) == 0.0202375845_a);
		
		CHECK(real(sharmonic_complex<complex>(5, -4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -4, 0.5, 1.5, 2.5)) == 0.0283530398_a);
		
		CHECK(real(sharmonic_complex<complex>(5, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -3, 3.0, 0.0, 0.0)) == -0.3459437191_a);
		CHECK(real(sharmonic_complex<complex>(5, -3, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -3, 0.5, 1.5, 2.5)) == -0.2358100379_a);
		
		CHECK(real(sharmonic_complex<complex>(5, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -2, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -2, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -2, 0.5, 1.5, 2.5)) == -0.3741630893_a);
		
		CHECK(real(sharmonic_complex<complex>(5, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -1, 3.0, 0.0, 0.0)) == 0.3202816486_a);
		CHECK(real(sharmonic_complex<complex>(5, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5, -1, 0.5, 1.5, 2.5)) == 0.0928071079_a);
		
		CHECK(real(sharmonic_complex<complex>(5,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 0, 0.0, 0.0, 2.0)) == 0.9356025796_a);
		CHECK(real(sharmonic_complex<complex>(5,	 0, 0.5, 1.5, 2.5)) == -0.282403036_a);
		
		CHECK(real(sharmonic_complex<complex>(5,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 1, 2.0, 0.0, 0.0)) == -0.3202816486_a);
		CHECK(real(sharmonic_complex<complex>(5,	 1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 1, 0.5, 1.5, 2.5)) == -0.0928071079_a);

		CHECK(real(sharmonic_complex<complex>(5,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 2, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 2, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 2, 0.5, 1.5, 2.5)) == -0.3741630893_a);

		CHECK(real(sharmonic_complex<complex>(5,	 3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 3, 2.0, 0.0, 0.0)) == 0.3459437191_a);
		CHECK(real(sharmonic_complex<complex>(5,	 3, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 3, 0.5, 1.5, 2.5)) == 0.2358100379_a);
		
		CHECK(real(sharmonic_complex<complex>(5,	 4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 4, 0.5, 1.5, 2.5)) == 0.0283530398_a);

		CHECK(real(sharmonic_complex<complex>(5,	 5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 5, 3.0, 0.0, 0.0)) == -0.4641322034_a);
		CHECK(real(sharmonic_complex<complex>(5,	 5, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(5,	 5, 0.5, 1.5, 2.5)) == -0.0202375845_a);

		CHECK(imag(sharmonic_complex<complex>(5, -5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -5, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -5, 0.0, 3.0, 0.0)) == -0.4641322034_a);
		CHECK(imag(sharmonic_complex<complex>(5, -5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -5, 0.5, 1.5, 2.5)) == 0.0007685159_a);
		
		CHECK(imag(sharmonic_complex<complex>(5, -4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -4, 0.5, 1.5, 2.5)) == 0.0972104222_a);
		
		CHECK(imag(sharmonic_complex<complex>(5, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -3, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -3, 0.0, 3.0, 0.0)) == -0.3459437191_a);
		CHECK(imag(sharmonic_complex<complex>(5, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -3, 0.5, 1.5, 2.5)) == 0.1632531032_a);
		
		CHECK(imag(sharmonic_complex<complex>(5, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -2, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -2, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -2, 0.5, 1.5, 2.5)) == -0.280622317_a);
		
		CHECK(imag(sharmonic_complex<complex>(5, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -1, 0.0, 3.0, 0.0)) == -0.3202816486_a);
		CHECK(imag(sharmonic_complex<complex>(5, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5, -1, 0.5, 1.5, 2.5)) == -0.2784213237_a);
		
		CHECK(imag(sharmonic_complex<complex>(5,	 0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 0, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 0, 0.5, 1.5, 2.5)) == (0.0_a).margin(1e-12));
		
		CHECK(imag(sharmonic_complex<complex>(5,	 1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 1, 0.0, 2.0, 0.0)) == -0.3202816486_a);
		CHECK(imag(sharmonic_complex<complex>(5,	 1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 1, 0.5, 1.5, 2.5)) == -0.2784213237_a);

		CHECK(imag(sharmonic_complex<complex>(5,	 2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 2, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 2, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 2, 0.5, 1.5, 2.5)) == 0.280622317_a);

		CHECK(imag(sharmonic_complex<complex>(5,	 3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 3, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 3, 0.0, 2.0, 0.0)) == -0.3459437191_a);
		CHECK(imag(sharmonic_complex<complex>(5,	 3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 3, 0.5, 1.5, 2.5)) == 0.1632531032_a);
		
		CHECK(imag(sharmonic_complex<complex>(5,	 4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 4, 0.5, 1.5, 2.5)) == -0.0972104222_a);

		CHECK(imag(sharmonic_complex<complex>(5,	 5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 5, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 5, 0.0, 3.0, 0.0)) == -0.4641322034_a);
		CHECK(imag(sharmonic_complex<complex>(5,	 5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(5,	 5, 0.5, 1.5, 2.5)) == 0.0007685159_a);
		
	}
	
	SECTION("complex l=6"){
		
		CHECK(real(sharmonic_complex<complex>(6, -6, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -6, 3.0, 0.0, 0.0)) == 0.4830841136_a);
		CHECK(real(sharmonic_complex<complex>(6, -6, 0.0, 3.0, 0.0)) == -0.4830841136_a);
		CHECK(real(sharmonic_complex<complex>(6, -6, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -6, 0.5, 1.5, 2.5)) == 0.0039660783_a);
		
		CHECK(real(sharmonic_complex<complex>(6, -5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -5, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -5, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -5, 0.5, 1.5, 2.5)) == 0.0616689186_a);
		
		CHECK(real(sharmonic_complex<complex>(6, -4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -4, 3.0, 0.0, 0.0)) == -0.3567812629_a);
		CHECK(real(sharmonic_complex<complex>(6, -4, 0.0, 3.0, 0.0)) == -0.3567812629_a);
		CHECK(real(sharmonic_complex<complex>(6, -4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -4, 0.5, 1.5, 2.5)) == 0.055920002_a);
		
		CHECK(real(sharmonic_complex<complex>(6, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -3, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -3, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -3, 0.5, 1.5, 2.5)) == -0.3357604604_a);
		
		CHECK(real(sharmonic_complex<complex>(6, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -2, 3.0, 0.0, 0.0)) == 0.3256952429_a);
		CHECK(real(sharmonic_complex<complex>(6, -2, 0.0, 3.0, 0.0)) == -0.3256952429_a);
		CHECK(real(sharmonic_complex<complex>(6, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -2, 0.5, 1.5, 2.5)) == -0.3707038567_a);
		
		CHECK(real(sharmonic_complex<complex>(6, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6, -1, 0.5, 1.5, 2.5)) == 0.024021896_a);
		
		CHECK(real(sharmonic_complex<complex>(6,	0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	0, 2.0, 0.0, 0.0)) == -0.3178460113_a);
		CHECK(real(sharmonic_complex<complex>(6,	0, 0.0, 2.0, 0.0)) == -0.3178460113_a);
		CHECK(real(sharmonic_complex<complex>(6,	0, 0.0, 0.0, 2.0)) == 1.0171072363_a);
		CHECK(real(sharmonic_complex<complex>(6,	0, 0.5, 1.5, 2.5)) == -0.4151458107_a);
		
		CHECK(real(sharmonic_complex<complex>(6,	1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	1, 0.5, 1.5, 2.5)) == -0.024021896_a);

		CHECK(real(sharmonic_complex<complex>(6,	2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	2, 2.0, 0.0, 0.0)) == 0.3256952429_a);
		CHECK(real(sharmonic_complex<complex>(6,	2, 0.0, 2.0, 0.0)) == -0.3256952429_a);
		CHECK(real(sharmonic_complex<complex>(6,	2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	2, 0.5, 1.5, 2.5)) == -0.3707038567_a);

		CHECK(real(sharmonic_complex<complex>(6,	3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	3, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	3, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	3, 0.5, 1.5, 2.5)) == 0.3357604604_a);
		
		CHECK(real(sharmonic_complex<complex>(6,	4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	4, 3.0, 0.0, 0.0)) == -0.3567812629_a);
		CHECK(real(sharmonic_complex<complex>(6,	4, 0.0, 3.0, 0.0)) == -0.3567812629_a);
		CHECK(real(sharmonic_complex<complex>(6,	4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	4, 0.5, 1.5, 2.5)) == 0.055920002_a);

		CHECK(real(sharmonic_complex<complex>(6,	5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	5, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	5, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	5, 0.5, 1.5, 2.5)) == -0.0616689186_a);

		CHECK(real(sharmonic_complex<complex>(6,	6, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	6, 3.0, 0.0, 0.0)) == 0.4830841136_a);
		CHECK(real(sharmonic_complex<complex>(6,	6, 0.0, 3.0, 0.0)) == -0.4830841136_a);
		CHECK(real(sharmonic_complex<complex>(6,	6, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(real(sharmonic_complex<complex>(6,	6, 0.5, 1.5, 2.5)) == 0.0039660783_a);

		CHECK(imag(sharmonic_complex<complex>(6, -6, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -6, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -6, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -6, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -6, 0.5, 1.5, 2.5)) == -0.0105461628_a);
		
		CHECK(imag(sharmonic_complex<complex>(6, -5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -5, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -5, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -5, 0.5, 1.5, 2.5)) == 0.0023418577_a);
		
		CHECK(imag(sharmonic_complex<complex>(6, -4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -4, 0.5, 1.5, 2.5)) == 0.1917257212_a);
		
		CHECK(imag(sharmonic_complex<complex>(6, -3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -3, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -3, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -3, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -3, 0.5, 1.5, 2.5)) == 0.2324495495_a);
		
		CHECK(imag(sharmonic_complex<complex>(6, -2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -2, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -2, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -2, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -2, 0.5, 1.5, 2.5)) == -0.2780278925_a);
		
		CHECK(imag(sharmonic_complex<complex>(6, -1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -1, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -1, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -1, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6, -1, 0.5, 1.5, 2.5)) == -0.072065688_a);
		
		CHECK(imag(sharmonic_complex<complex>(6,	0, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	0, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	0, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	0, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	0, 0.5, 1.5, 2.5)) == (0.0_a).margin(1e-12));
		
		CHECK(imag(sharmonic_complex<complex>(6,	1, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	1, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	1, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	1, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	1, 0.5, 1.5, 2.5)) == -0.072065688_a);

		CHECK(imag(sharmonic_complex<complex>(6,	2, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	2, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	2, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	2, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	2, 0.5, 1.5, 2.5)) == 0.2780278925_a);

		CHECK(imag(sharmonic_complex<complex>(6,	3, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	3, 2.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	3, 0.0, 2.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	3, 0.0, 0.0, 2.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	3, 0.5, 1.5, 2.5)) == 0.2324495495_a);
		
		CHECK(imag(sharmonic_complex<complex>(6,	4, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	4, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	4, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	4, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	4, 0.5, 1.5, 2.5)) == -0.1917257212_a);

		CHECK(imag(sharmonic_complex<complex>(6,	5, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	5, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	5, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	5, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	5, 0.5, 1.5, 2.5)) == 0.0023418577_a);

		CHECK(imag(sharmonic_complex<complex>(6,	6, 0.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	6, 3.0, 0.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	6, 0.0, 3.0, 0.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	6, 0.0, 0.0, 3.0)) == (0.0_a).margin(1e-12));
		CHECK(imag(sharmonic_complex<complex>(6,	6, 0.5, 1.5, 2.5)) == 0.0105461628_a);
		
	}

}
#endif
