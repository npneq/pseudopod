/* -*- indent-tabs-mode: t -*- */

#ifndef PSEUDOPOD__PATH
#define PSEUDOPOD__PATH

// Copyright (C) 2019-2023 Lawrence Livermore National Security, LLC., Xavier Andrade
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at https://mozilla.org/MPL/2.0/.

#include <pseudopod_config.h>

#include <string>

namespace pseudopod {
struct path {
	static std::string share(){ return PSEUDOPOD_PREFIX_DIR + std::string("/share/pseudopod/") ; }
	static std::string unit_tests_data(){ return share() + std::string("/unit_tests_data/"); }
};
}

#endif

#ifdef INQ__PATH_UNIT_TEST
#undef INQ__PATH_UNIT_TEST

#include <catch2/catch_all.hpp>

TEST_CASE(INQ_TEST_FILE, INQ_TEST_TAG) {
	using namespace Catch::literals;
	using Catch::Approx;

	SECTION("Share path"){
    CHECK(pseudopod::path::share() == PSEUDOPOD_PREFIX_DIR + std::string("/share/pseudopod/"));
  }
	
}

#endif
